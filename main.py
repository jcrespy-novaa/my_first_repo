import pandas as pd

def calculate_dcf(cash_flow, discount_rate):
    """
    Calculates the cumulative discounted cash flow of 
    cash_flow at discount_rate.
    Expects a list of float as cash flows and a 
    percentage discount_rate.
    """
    return sum([
        value / (1. + discount_rate)**(index + 1) 
        for index, value in enumerate(cash_flow)
        ])


def get_dcf_table(cash_flow, discount_rate):
    """
    Returns a dataframe of discounted cash flows 
    of cash_flow at discount_rate
    """
    return pd.DataFrame([{'year': index, 
             'cash_flow': value,
             'discounted_cash_flow': value / (1. + discount_rate)**(index + 1)
             } for index, value in enumerate(cash_flow)])


def dcf_multiple_discount_rates(cash_flow, list_of_discount_rates):
    """
    Returns a dataframe of discounted cash flows 
    of cash_flow for all discount rates
    """
    return pd.DataFrame([{
        **{'year': index, 
         'cash_flow': value},
        **{f'discounted_cash_flow_{discount_rate}': value / (1. + discount_rate)**(index + 1) for discount_rate in list_of_discount_rates}
        } for index, value in enumerate(cash_flow)])

#Example to check if everything works:
cash_flow = [2000., 2500., 3000., 3000., 3000., 3500., 3500.]
discount_rate = 0.1
dcf = round(calculate_dcf(cash_flow, discount_rate), 2)
dcf_table = get_dcf_table(cash_flow, discount_rate)
print(f"The DCF of {cash_flow} at a {discount_rate} discount rate is {dcf}")
print(f"The DCF table of {cash_flow} at a {discount_rate} discount rate is \n{dcf_table}")
list_of_discount_rates = [0.1, 0.12, 0.15]
dcf_multiple = dcf_multiple_discount_rates(cash_flow, list_of_discount_rates)
print(f"The DCF table of {cash_flow} at a {list_of_discount_rates} discount rate is \n{dcf_multiple}")
